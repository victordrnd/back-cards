package com.sp.model;

public class Card  {

    private String name;
	private String description;
    private String family;
	private String imgUrl;
    private String affinity;
    private Integer hp; 
    private Integer energy; 
    private Integer attack; 
    private Integer defense;
    
	public Card() {
		this.family = "";
		this.description = "";
		this.name = "";
		this.imgUrl="";
        this.affinity = "";
        this.hp = 0;
        this.energy = 0;
        this.attack = 0;
        this.defense = 0;
	}
	public Card(String name,String description,String family, String imgUrl, String affinity, Integer hp, Integer energy, Integer attack, Integer defense) {
		this.description = description;
		this.family = family;
        this.affinity = affinity;
		this.name = name;
		this.imgUrl=imgUrl;
        this.hp = hp;
        this.energy = energy;
        this.attack = attack;
        this.defense = defense;
	}


  // GETTER AND SETTER

  public String getImgUrl() {
      return imgUrl;
  }
  public String getName() {
      return name;
  }
  public void setName(String name) {
      this.name = name;
  }
  public void setImgUrl(String imgUrl) {
      this.imgUrl = imgUrl;
  }

  public String getDescription(){
      return this.description;
  }


  public String getFamily() {
    return family;
}

public String getAffinity() {
    return affinity;
}

public Integer getHp() {
    return hp;
}
public Integer getDefense() {
    return defense;
}

public Integer getAttack() {
    return attack;
}
public Integer getEnergy() {
    return energy;
}

}