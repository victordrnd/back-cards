package com.sp.model;

public class CardFormDTO {

    private String name;
	private String description;
    private String family;
	private String imgUrl;
    private String affinity;
    private Integer hp; 
    private Integer energy; 
    private Integer attack; 
    private Integer defense;

	public CardFormDTO() {
		this.family = "";
		this.description = "";
		this.name = "";
		this.imgUrl="";
        this.affinity ="";
        this.hp = 0;
        this.energy = 0;
        this.attack = 0;
        this.defense = 0;
	}
	public CardFormDTO(String name,String description,String family, String imgUrl, String affinity, Integer hp, Integer energy, Integer attack, Integer defense) {
		this.description = description;
		this.family = family;
        this.affinity = affinity;
		this.name = name;
		this.imgUrl=imgUrl;
        this.hp = hp;
        this.energy = energy;
        this.attack = attack;
        this.defense = defense;
	}

  // GETTER AND SETTER

  public String getImgUrl() {
      return imgUrl;
  }
  public String getName() {
      return name;
  }
  public void setName(String name) {
      this.name = name;
  }
  public void setImgUrl(String imgUrl) {
      this.imgUrl = imgUrl;
  }

  public String getDescription() {
      return description;
  }


  public void setDescription(String description) {
      this.description = description;
  }
  public String getFamily() {
      return family;
  }
  public void setFamily(String family) {
      this.family = family;
  }

  public String getAffinity() {
      return affinity;
  }
  public void setAffinity(String affinity) {
      this.affinity = affinity;
  }

  public Integer getHp() {
      return hp;
  }
  public void setHp(Integer hp) {
      this.hp = hp;
  }
  public Integer getDefense() {
      return defense;
  }
  public void setDefense(Integer defense) {
      this.defense = defense;
  }

  public void setEnergy(Integer energy) {
      this.energy = energy;
  }

  public void setAttack(Integer attack) {
      this.attack = attack;
  }

  public Integer getAttack() {
      return attack;
  }
  public Integer getEnergy() {
      return energy;
  }
}