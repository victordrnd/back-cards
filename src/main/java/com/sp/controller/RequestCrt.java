package com.sp.controller;

import com.sp.model.Card;
import com.sp.model.CardFormDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RequestCrt {

    @Value("${welcome.message}")
    private String message;


    @RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
    public String index(Model model) {

        model.addAttribute("message", message);
        return "index";
    }

    @RequestMapping(value = { "/view" }, method = RequestMethod.GET)
    public String view(Model model) {
        CardDao cardDao = new CardDao();
        model.addAttribute("myCard", cardDao.getRandomCard());
        return "cardView";
    }

    @RequestMapping(value = { "/addCard" }, method = RequestMethod.GET)
    public String addcard(Model model) {
        CardFormDTO poneyForm = new CardFormDTO();
        model.addAttribute("cardForm", poneyForm);
        return "cardForm";
    }

    @RequestMapping(value = { "/addCard" }, method = RequestMethod.POST)
    public String addcard(Model model, @ModelAttribute("cardForm") CardFormDTO cardForm) {
        CardDao cardDao = new CardDao();
        Card p = cardDao.addCard(cardForm.getName(), cardForm.getDescription(), cardForm.getFamily(),
                cardForm.getImgUrl(), cardForm.getAffinity(), cardForm.getHp(), cardForm.getEnergy(), cardForm.getAttack(), cardForm.getDefense());
        model.addAttribute("myCard", p);
        return "cardView";
    }

    @RequestMapping(value = { "/list" }, method = RequestMethod.GET)
    public String viewList(Model model) {
        CardDao cardDao = new CardDao();
        model.addAttribute("cardList", cardDao.getCardList());
        return "cardViewList";
    }

}
