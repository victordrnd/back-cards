package com.sp.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Service;

import com.sp.model.Card;

@Service
public class CardDao {
	private List<Card> cardList;
	private Random randomGenerator;

	public CardDao() {
		cardList=new ArrayList<>();
		randomGenerator = new Random();
		createCardList();
	}

	private void createCardList() {
		// Card p1=new Card("John", "pink", new Family(1,"super pink"), "http://ekladata.com/9-cPSlYvrenNHMVawFmf_gLx8Jw.gif");
		// Card p2=new Card("Roberto", "blue", new Family(1,"super lazy"), "http://ekladata.com/JEVyY9DkwX4vVkakeBfikSyPROA.gif");
		// Card p3=new Card("Anna", "orange", new Family(1,"super music girl"), "http://ekladata.com/fMJl--_v-3CmisaynTHju1DMeXE.gif");
		// Card p4=new Card("Angry Joe", "purple", new Family(1,"super angry power"), "http://ekladata.com/AmbNNNvv-4YFEMZR8XD8e54WoHc.gif");
		// Card p5=new Card("Ursula", "green", new Family(1,"super cloning power"), "http://ekladata.com/CXJhi2YLUbNz6__e0Ct6ZP-XOds.gif");

		// cardList.add(p1);
		// cardList.add(p2);
		// cardList.add(p3);
		// cardList.add(p4);
		// cardList.add(p5);
	}
	public List<Card> getCardList() {
		return this.cardList;
	}
	public Card getCardByName(String name){
		for (Card cardBean : cardList) {
			if(cardBean.getName().equals(name)){
				return cardBean;
			}
		}
		return null;
	}
	public Card getRandomCard(){
		int index=randomGenerator.nextInt(this.cardList.size());
		return this.cardList.get(index);
	}

	public Card addCard(String name, String desc, String family, String imgUrl, String affinity, Integer hp,Integer energy, Integer attack, Integer defense) {
		Card c= new Card(name, desc, family, imgUrl, affinity, hp, energy, attack, defense);
		this.cardList.add(c);
		return c;
	}
}

